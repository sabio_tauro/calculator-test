const OPERATORS_REGEX = [
        /(\-?\d+)(\/)(\-?\d+)/,
        /(\-?\d+)(\*)(\-?\d+)/,
        /(\-?\d+)(\-)(\-?\d+)/,
        /(\-?\d+)(\+)(\-?\d+)/
    ],
    PARENTHESES_REGEX = /\(.*\)/,
    OPERATORS_FUNCTIONS = {
        '/'(a, b){
            return a/b;
        },
        '*'(a, b){
            return a*b;
        },
        '-'(a, b){
            return a + -b;
        },
        '+'(a, b){
            return a+b;
        }
    };

var expression = '((4 + 2) / 2 -1 + 2 - 2 + 3 - 4 +10) * 2',
    currentOperator = 0,
    calculator = {
        calculate(expression){
            if (/[a-z]/g.test(expression)) {
                console.log('Invalid input');
                return;
            }
            // Remove all whitespaces from the income string
            expression = expression.replace(/\s/g, '');
            // Start Looking for parenthese in string
            let tempResult = calculator.findParentheses(expression, PARENTHESES_REGEX);;
            
            if(calculator.isOperatorsLeft(tempResult)){
                currentOperator = 0;
                return calculator.calculate(tempResult);
            } else{
                return tempResult;
            }
        },
        isOperatorsLeft(string){
            return OPERATORS_REGEX.some(operator=>{
                return operator.test(string);
            });
        },
        continueSearch(tempResult){
            if(calculator.isOperatorsLeft(tempResult)){
                currentOperator = 0;
                return calculator.calculateString(tempResult, OPERATORS_REGEX[currentOperator]);
            } else{
                return  tempResult;
            }
        },
        handlingParentheses(parenthesesSubstring, index, originalSring){
            // Remove parentheses from substring
            croppedSubstring = parenthesesSubstring.substr(1, parenthesesSubstring.length-2);
            // Search parentheses in current substring
            let tempResult = calculator.findParentheses(croppedSubstring, PARENTHESES_REGEX);
            return calculator.continueSearch(tempResult);
        },
        findParentheses(string, regex){
            // Check if in the string does not contain a parentheses, continue with calculation
            if(!regex.test(string)){
                let tempResult = calculator.calculateString(string, OPERATORS_REGEX[currentOperator]);
                
                return calculator.continueSearch(tempResult);
            }
            
            // Handling expression in parentheses
            let tempResult = string.replace(regex, calculator.handlingParentheses);
            return calculator.continueSearch(tempResult);
        },
        calculateString(string, regex){
            // Check string for operator we looking
            if( !regex.test(string) ){
                // If there are none, continue with next operator
                currentOperator++;
                
                if(OPERATORS_REGEX[currentOperator]){
                    // Run calculation with new operator
                    return calculator.calculateString(string, OPERATORS_REGEX[currentOperator]);
                } else{
                    // If there no more operators, return string
                    return string;
                }
            }
            let tempResult = string.replace(regex, calculator.replacer);
            return calculator.continueSearch(tempResult);
        },
        replacer(substringToReplace, firstOperand, operator, secondOperand, index, originalString){
            // Calculation of two numbers from the substring with current operator
            return OPERATORS_FUNCTIONS[operator](parseInt(firstOperand), parseInt(secondOperand));
        }
    };

console.log('Final Result', calculator.calculate(expression));
